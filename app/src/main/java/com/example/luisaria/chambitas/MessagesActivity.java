package com.example.luisaria.chambitas;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.InputStream;
import java.net.URL;

import static com.example.luisaria.chambitas.R.id.imageView;

public class MessagesActivity extends AppCompatActivity {

  ImageView image;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_messages);
    image = (ImageView) findViewById(imageView);
    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
    System.out.println();
    Toast.makeText(this, "Refreshed token: " + refreshedToken, Toast.LENGTH_LONG).show();
    ((EditText)findViewById(R.id.editText)).setText(refreshedToken);
  }

  public void loadImage1(View v){
    loadImageWithAThread("https://upload.wikimedia.org/wikipedia/en/f/f9/Death_star1.png");
  }

  public void loadImageWithAThread(final String url){

//    new Thread(new Runnable() {
//      public void run() {
//        final Bitmap bitmap =
//          loadImageFromNetwork(url);
//      }};
  }

      private Bitmap loadImageFromNetwork(String url) {
        try {
          Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
          return bitmap;
        } catch (Exception e) {
          e.printStackTrace();
        }
        return null;
      }

      @Override
      public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
      }

      @Override
      public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(MenuOptions.options(item, this));
        return true;
      }

    }

