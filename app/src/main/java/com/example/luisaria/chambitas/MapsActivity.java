package com.example.luisaria.chambitas;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
  double longitude, latitude;
  private GoogleMap mMap;
  GPSTracker gpsTracker;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);

    gpsTracker = new GPSTracker(this);
    requestPermissions();

    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      Toast.makeText(this, "Location permissions are required", Toast.LENGTH_SHORT).show();
      return;
    }

    updateLatLon();
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

  }

  public void updateLatLon(){
    if (gpsTracker.getIsGPSTrackingEnabled())
    {
      longitude = gpsTracker.getLongitude();
      latitude = gpsTracker.getLatitude();
    }
    Toast.makeText(this, "LAT_LON: "+latitude+"_"+longitude, Toast.LENGTH_SHORT).show();
  }

  public void requestPermissions(){
    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      try{
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},2);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},3);
      }
      catch(Exception ex){
        Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();

      }
    }
  }

  public void updateLocation(View v){
    if (gpsTracker.getIsGPSTrackingEnabled())
    {
      longitude = gpsTracker.getLongitude();
      latitude = gpsTracker.getLatitude();
    }
    LatLng you_are_here = new LatLng(latitude, longitude);
    mMap.addMarker(new MarkerOptions().position(you_are_here).title("You are here"));
    mMap.moveCamera(CameraUpdateFactory.newLatLng(you_are_here));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    startActivity(MenuOptions.options(item, this));
    return true;
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    LatLng you_are_here = new LatLng(latitude, longitude);
    mMap.addMarker(new MarkerOptions().position(you_are_here).title("You are here"));
    mMap.moveCamera(CameraUpdateFactory.newLatLng(you_are_here));
    mMap.setMinZoomPreference(10);
  }
}
