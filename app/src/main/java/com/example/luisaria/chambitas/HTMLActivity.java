package com.example.luisaria.chambitas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;

public class HTMLActivity extends AppCompatActivity {

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_html);
    createHTML();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    startActivity(MenuOptions.options(item, this));
    return true;
  }

  public void createHTML(){

    // Obtain reference to the WebView holder
    WebView webview = (WebView) this.findViewById(R.id.webview);

    // Get the settings
    WebSettings webSettings = webview.getSettings();

    // Enable Javascript for user interaction clicks
    webSettings.setJavaScriptEnabled(true);

    // Display Zoom Controles
    webSettings.setBuiltInZoomControls(true);
    webview.requestFocusFromTouch();

    // Set the client
    webview.setWebViewClient(new WebViewClient());
    webview.setWebChromeClient(new WebChromeClient());

    // Load the URL
    webview.loadUrl("file:///android_asset/localHTML.html");
  }
}
