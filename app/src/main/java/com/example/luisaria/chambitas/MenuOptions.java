package com.example.luisaria.chambitas;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 * Created by luisaria on 09/02/2017.
 */
public class MenuOptions {
  public static Intent options(MenuItem item, Activity activity){
    Intent intent;
    switch (item.getItemId()) {
      case R.id.home_id:
        return new Intent(activity, HomeActivity.class);
      case R.id.messages_id:
        return  new Intent(activity, MapsActivity.class);
      case R.id.categories_id:
        return new Intent(activity, CategoriesActivity.class);
      case R.id.saved_id:
        return new Intent(activity, SavedActivity.class);
      case R.id.user_id:
        return new Intent(activity, HTMLActivity.class);
      case R.id.cat_1_id:
        return new Intent(activity, MessagesActivity.class);
      default:
        return new Intent(activity, HomeActivity.class);
    }
  }
}
