package com.example.luisaria.chambitas;

/**
 * Created by luisaria on 01/02/2017.
 */
public class serverList {
  int id;
  String title;

  public serverList(int id, String title){
    this.id = id;
    this.title = title;
  }

  @Override
  public String toString() {
    return title;
  }

  public int getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }
}
