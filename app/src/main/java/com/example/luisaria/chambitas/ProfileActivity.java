package com.example.luisaria.chambitas;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ProfileActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  public void makeCall(View v){

  }

  public static String retrieve_REST(String host, int port , String path) throws IOException {
    URL url = new URL("http", host, port, path);
    URLConnection conn = url.openConnection();
    conn.setDoInput(true);
    conn.setAllowUserInteraction(true); // useless but harmless
    conn.connect();

    StringBuilder sb = new StringBuilder();
    BufferedReader in = new BufferedReader(
      new InputStreamReader(conn.getInputStream()));
    String line;
    while ((line = in.readLine()) != null) {
      sb.append(line);
    }
    in.close();
    return sb.toString();
  }

  private class RestCall extends AsyncTask<String, Void, JSONObject> {
    protected JSONObject doInBackground(String... params) {
      JSONObject anaResponse = null;
      try {
        anaResponse = new JSONObject(retrieve_REST("0179713.upweb.site", 8080, "/ana"));
      } catch (IOException e) {
        e.printStackTrace();
      } catch (JSONException e) {
        e.printStackTrace();
      }
      return anaResponse;
    }

    protected void onPostExecute(JSONObject anaResponse) {
      try {
        JSONArray result = anaResponse.getJSONArray("result");
        for(int i=0; i<result.length(); i++){
          JSONObject item = result.getJSONObject(i);
          int id = item.getInt("id");
          String title = item.getString("title");
          System.out.println("ID: " + id + " Title:" + title);
        }
      } catch (JSONException e) {
        e.printStackTrace();
      }

    }
  }


}
