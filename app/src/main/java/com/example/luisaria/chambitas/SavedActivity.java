package com.example.luisaria.chambitas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

public class SavedActivity extends AppCompatActivity{

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_saved);
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  public void startService(View v){
    Intent intentService = new Intent(this, MusicPlayerService.class);
    startService(intentService);
  }

  public void stopService(View v){
    Intent intentService = new Intent(this, MusicPlayerService.class);
    stopService(intentService);
  }
}
