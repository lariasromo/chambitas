package com.example.luisaria.chambitas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import static android.support.v4.content.ContextCompat.startActivity;

public class HomeActivity extends AppCompatActivity {
  Camera cam = null;
  Camera.Parameters camParams;
  FlashLightUtilForL flashlight;

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    Context context = getApplicationContext();
    flashlight = new FlashLightUtilForL(context);
    if(  context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)  ){
      Toast.makeText(this, "Flashlight is Supported", Toast.LENGTH_LONG).show();
    }
    else{
      Toast.makeText(this, "Flashlight is not Supported", Toast.LENGTH_LONG).show();
    }
    Button button = (Button) findViewById(R.id.button6);
    registerForContextMenu(button);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    getSupportActionBar().setDisplayUseLogoEnabled(true);
  }

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  public void turnOff(View v) {
    flashlight.turnOffFlashLight();
  }

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  public void turnOn(View v){

    flashlight.turnOnFlashLight();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    startActivity(MenuOptions.options(item, this));
    return true;
  }



  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    getMenuInflater().inflate(R.menu.main_menu , menu);
  }

  public void toast(View v){
    Toast.makeText(this, "Hello there", Toast.LENGTH_LONG).show();
  }

  public void snackbar(View v){
    Snackbar.make(v, "Hello There", Snackbar.LENGTH_LONG).setAction("Action", null).show();

  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    Intent intent;
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

    switch (item.getItemId()) {
      case R.id.home_id:
        intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        return true;
      case R.id.messages_id:
        intent = new Intent(this, MessagesActivity.class);
        startActivity(intent);
        return true;
      case R.id.categories_id:
        intent = new Intent(this, CategoriesActivity.class);
        startActivity(intent);
        return true;
      case R.id.user_id:
        intent = new Intent(this, HTMLActivity.class);
        startActivity(intent);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void alert(View v){
    showDialog("Hello There");
  }

  public void showDialog(String message){
    AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
    builder1.setMessage(message);
    builder1.setCancelable(true);
    builder1.setPositiveButton(
      "SI",
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {dialog.cancel();
        }
      });
    builder1.setNegativeButton(
      "NO",
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          dialog.cancel();
        }
      });
    AlertDialog alert11 = builder1.create();
    alert11.show();

  }

  public void popUpMenu(View v) {
    Button button = (Button) findViewById(R.id.button6);
    //Creating the instance of PopupMenu
    PopupMenu popup = new PopupMenu(HomeActivity.this, button);
    //Inflating the Popup using xml file
    popup.getMenuInflater()
      .inflate(R.menu.main_menu, popup.getMenu());

    //registering popup with OnMenuItemClickListener
    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
      public boolean onMenuItemClick(MenuItem item) {
        Toast.makeText(
          HomeActivity.this,
          "You Clicked : " + item.getTitle(),
          Toast.LENGTH_SHORT
        ).show();
        return true;
      }
    });

    popup.show(); //showing popup menu
  }
}
